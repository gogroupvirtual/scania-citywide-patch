# Install instructions
> Original bus needs to be installed before
1. Download the latest version from here: https://gitlab.com/gogroupvirtual/scania-citywide-patch/-/archive/master/scania-citywide-patch-master.zip
2. Unpack the contents of `scania-citywide-patch-master` folder into your OMSI folder. It will ask to overwrite, say **Yes**.

# What does this patchpack change
- Fix Solo (12LF) buses collision @gameburro
- Replace 12LF used engine and gearbox to make it feel less overpowered @volvorider
- Adjust 18LF versions to not feel overpowered @volvorider